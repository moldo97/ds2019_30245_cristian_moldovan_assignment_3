package com.example.demo.controller;

import com.example.demo.dto.MedicationDTO;
import com.example.demo.dto.MedicationViewDTO;
import com.example.demo.service.MedicationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
@RequestMapping(value = "/medication")
public class MedicationController {

    private final MedicationService medicationService;

    @Autowired
    public MedicationController(MedicationService medicationService) {
        this.medicationService = medicationService;
    }

    @GetMapping(value = "/{id}")
    public MedicationDTO findById(@PathVariable("id") Long id){
        return medicationService.findMedicationById(id);
    }

    @GetMapping()
    public List<MedicationDTO> findAll(){
        return medicationService.findAllMedication();
    }

    @PostMapping()
    public Long createMedication(@RequestBody MedicationDTO medicationDTO){
        return medicationService.create(medicationDTO);
    }

    @PutMapping("/{id}")
    public Long updateMedication(@RequestBody MedicationViewDTO medicationViewDTO, @PathVariable("id") Long id){
        return medicationService.update(medicationViewDTO,id);
    }

    @DeleteMapping("/{id}")
    public Map<String, Boolean> deleteMedication(@PathVariable("id") Long id){
        return medicationService.delete(id);
    }
}
