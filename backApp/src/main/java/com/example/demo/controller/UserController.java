package com.example.demo.controller;

import com.example.demo.dto.CaregiverDTO;
import com.example.demo.dto.CaregiverViewDTO;
import com.example.demo.dto.LoginDTO;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@CrossOrigin(origins =  "http://localhost:4200")
@RequestMapping(value = "/user")
public class UserController {

    private final UserService userService;

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    //Caregiver role
    //////////////////////////////////////////////////////////////

    @GetMapping(value = "/caregiver/{id}")
    public CaregiverDTO findCaregiverById(@PathVariable("id") Long id) { return userService.findUserById(id); }

    @GetMapping(value = "/{username}")
    public CaregiverViewDTO findUserByUsername(@PathVariable("username") String username) { return userService.findUserByUsername(username); }

    @GetMapping(value = "/caregiver")
    public List<CaregiverDTO> findAllCaregiver(){
        return userService.findAllCaregiver();
    }

    @PostMapping(value = "/login")
    public LoginDTO login(@RequestBody LoginDTO loginDTO) { return userService.login(loginDTO); }

    @PostMapping(value = "/caregiver")
    public Long createCaregiver(@RequestBody CaregiverDTO caregiverDTO){
        return userService.create(caregiverDTO);
    }

    @PutMapping(value = "/caregiver/{id}")
    public Long updateCaregiver(@RequestBody CaregiverViewDTO caregiverViewDTO,  @PathVariable("id") Long id){
        return userService.update(caregiverViewDTO,id);
    }

    @DeleteMapping(value = "/caregiver/{id}")
    public Map<String, Boolean> deleteCaregiver(@PathVariable("id") Long id){
        return userService.delete(id);
    }

    //////////////////////////////////////////////////////////////

}
