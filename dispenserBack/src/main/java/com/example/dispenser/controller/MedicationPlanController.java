package com.example.dispenser.controller;

import com.example.dispenser.grpc.MedicationPlanResponse;
import com.example.dispenser.service.DispenserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin(origins = "http://localhost:4200")
public class MedicationPlanController {

    private final DispenserService dispenserService;

    @Autowired
    public MedicationPlanController(DispenserService dispenserService) {
        this.dispenserService = dispenserService;
    }

    @GetMapping(value = "/medications")
    public ResponseEntity findAll(){
        return ResponseEntity.ok(dispenserService.getGoodIntakes());



    }
}
