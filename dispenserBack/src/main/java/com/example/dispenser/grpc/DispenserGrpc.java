package com.example.dispenser.grpc;

import static io.grpc.MethodDescriptor.generateFullMethodName;
import static io.grpc.stub.ClientCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ClientCalls.asyncClientStreamingCall;
import static io.grpc.stub.ClientCalls.asyncServerStreamingCall;
import static io.grpc.stub.ClientCalls.asyncUnaryCall;
import static io.grpc.stub.ClientCalls.blockingServerStreamingCall;
import static io.grpc.stub.ClientCalls.blockingUnaryCall;
import static io.grpc.stub.ClientCalls.futureUnaryCall;
import static io.grpc.stub.ServerCalls.asyncBidiStreamingCall;
import static io.grpc.stub.ServerCalls.asyncClientStreamingCall;
import static io.grpc.stub.ServerCalls.asyncServerStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnaryCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedStreamingCall;
import static io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall;

/**
 * <pre>
 * The greeting service definition.
 * </pre>
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.25.0)",
    comments = "Source: dispenser.proto")
public final class DispenserGrpc {

  private DispenserGrpc() {}

  public static final String SERVICE_NAME = "grpc.Dispenser";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.example.dispenser.grpc.MedicationPlanRequest,
      com.example.dispenser.grpc.MedicationPlanResponse> getGetMedicationPlansMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "GetMedicationPlans",
      requestType = com.example.dispenser.grpc.MedicationPlanRequest.class,
      responseType = com.example.dispenser.grpc.MedicationPlanResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
  public static io.grpc.MethodDescriptor<com.example.dispenser.grpc.MedicationPlanRequest,
      com.example.dispenser.grpc.MedicationPlanResponse> getGetMedicationPlansMethod() {
    io.grpc.MethodDescriptor<com.example.dispenser.grpc.MedicationPlanRequest, com.example.dispenser.grpc.MedicationPlanResponse> getGetMedicationPlansMethod;
    if ((getGetMedicationPlansMethod = DispenserGrpc.getGetMedicationPlansMethod) == null) {
      synchronized (DispenserGrpc.class) {
        if ((getGetMedicationPlansMethod = DispenserGrpc.getGetMedicationPlansMethod) == null) {
          DispenserGrpc.getGetMedicationPlansMethod = getGetMedicationPlansMethod =
              io.grpc.MethodDescriptor.<com.example.dispenser.grpc.MedicationPlanRequest, com.example.dispenser.grpc.MedicationPlanResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.SERVER_STREAMING)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "GetMedicationPlans"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.dispenser.grpc.MedicationPlanRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.example.dispenser.grpc.MedicationPlanResponse.getDefaultInstance()))
              .setSchemaDescriptor(new DispenserMethodDescriptorSupplier("GetMedicationPlans"))
              .build();
        }
      }
    }
    return getGetMedicationPlansMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static DispenserStub newStub(io.grpc.Channel channel) {
    return new DispenserStub(channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static DispenserBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    return new DispenserBlockingStub(channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static DispenserFutureStub newFutureStub(
      io.grpc.Channel channel) {
    return new DispenserFutureStub(channel);
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static abstract class DispenserImplBase implements io.grpc.BindableService {

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void getMedicationPlans(com.example.dispenser.grpc.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<com.example.dispenser.grpc.MedicationPlanResponse> responseObserver) {
      asyncUnimplementedUnaryCall(getGetMedicationPlansMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetMedicationPlansMethod(),
            asyncServerStreamingCall(
              new MethodHandlers<
                com.example.dispenser.grpc.MedicationPlanRequest,
                com.example.dispenser.grpc.MedicationPlanResponse>(
                  this, METHODID_GET_MEDICATION_PLANS)))
          .build();
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DispenserStub extends io.grpc.stub.AbstractStub<DispenserStub> {
    private DispenserStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DispenserStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DispenserStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DispenserStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public void getMedicationPlans(com.example.dispenser.grpc.MedicationPlanRequest request,
        io.grpc.stub.StreamObserver<com.example.dispenser.grpc.MedicationPlanResponse> responseObserver) {
      asyncServerStreamingCall(
          getChannel().newCall(getGetMedicationPlansMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DispenserBlockingStub extends io.grpc.stub.AbstractStub<DispenserBlockingStub> {
    private DispenserBlockingStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DispenserBlockingStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DispenserBlockingStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DispenserBlockingStub(channel, callOptions);
    }

    /**
     * <pre>
     * Sends a greeting
     * </pre>
     */
    public java.util.Iterator<com.example.dispenser.grpc.MedicationPlanResponse> getMedicationPlans(
        com.example.dispenser.grpc.MedicationPlanRequest request) {
      return blockingServerStreamingCall(
          getChannel(), getGetMedicationPlansMethod(), getCallOptions(), request);
    }
  }

  /**
   * <pre>
   * The greeting service definition.
   * </pre>
   */
  public static final class DispenserFutureStub extends io.grpc.stub.AbstractStub<DispenserFutureStub> {
    private DispenserFutureStub(io.grpc.Channel channel) {
      super(channel);
    }

    private DispenserFutureStub(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected DispenserFutureStub build(io.grpc.Channel channel,
        io.grpc.CallOptions callOptions) {
      return new DispenserFutureStub(channel, callOptions);
    }
  }

  private static final int METHODID_GET_MEDICATION_PLANS = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final DispenserImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(DispenserImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_MEDICATION_PLANS:
          serviceImpl.getMedicationPlans((com.example.dispenser.grpc.MedicationPlanRequest) request,
              (io.grpc.stub.StreamObserver<com.example.dispenser.grpc.MedicationPlanResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class DispenserBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    DispenserBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.example.dispenser.grpc.DispenserProto.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("Dispenser");
    }
  }

  private static final class DispenserFileDescriptorSupplier
      extends DispenserBaseDescriptorSupplier {
    DispenserFileDescriptorSupplier() {}
  }

  private static final class DispenserMethodDescriptorSupplier
      extends DispenserBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    DispenserMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (DispenserGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new DispenserFileDescriptorSupplier())
              .addMethod(getGetMedicationPlansMethod())
              .build();
        }
      }
    }
    return result;
  }
}
