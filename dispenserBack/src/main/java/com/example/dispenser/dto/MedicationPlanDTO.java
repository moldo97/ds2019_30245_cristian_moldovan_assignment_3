package com.example.dispenser.dto;

import java.io.Serializable;

public class MedicationPlanDTO implements Serializable  {

    private String startEndTime ;
    private String medicationName ;
    private String patientName ;
    private String intake_start ;
    private String intake_end;

    public MedicationPlanDTO(String startEndTime, String medicationName, String patientName, String intake_start, String intake_end) {
        this.startEndTime = startEndTime;
        this.medicationName = medicationName;
        this.patientName = patientName;
        this.intake_start = intake_start;
        this.intake_end = intake_end;
    }

    public MedicationPlanDTO() {
    }

    public String getStartEndTime() {
        return startEndTime;
    }

    public void setStartEndTime(String startEndTime) {
        this.startEndTime = startEndTime;
    }

    public String getMedicationName() {
        return medicationName;
    }

    public void setMedicationName(String medicationName) {
        this.medicationName = medicationName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getIntake_start() {
        return intake_start;
    }

    public void setIntake_start(String intake_start) {
        this.intake_start = intake_start;
    }

    public String getIntake_end() {
        return intake_end;
    }

    public void setIntake_end(String intake_end) {
        this.intake_end = intake_end;
    }
}
