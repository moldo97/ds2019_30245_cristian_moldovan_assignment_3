package com.example.dispenser.service;

import com.example.dispenser.dto.MedicationPlanDTO;
import com.example.dispenser.grpc.DispenserGrpc;
import com.example.dispenser.grpc.MedicationPlanRequest;
import com.example.dispenser.grpc.MedicationPlanResponse;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@EnableAsync
@EnableScheduling
@Service
public class DispenserService {

    private final ManagedChannel channel;
    private final DispenserGrpc.DispenserBlockingStub blockingStub;

    List<MedicationPlanResponse> med;

    public DispenserService() {
        this.channel = ManagedChannelBuilder.forAddress("localhost", 8088)
                .usePlaintext()
                .build();
        this.blockingStub = DispenserGrpc.newBlockingStub(channel);
        med = new ArrayList<>();
    }

    @Scheduled(fixedRate = 10000)
    public void getMedicationPlans(){
        med.clear();
        MedicationPlanRequest request = MedicationPlanRequest.newBuilder()
                .setTime(new Date().toString()).build();
        Iterator<MedicationPlanResponse> response;
        try {
            response = blockingStub.getMedicationPlans(request);
        } catch (StatusRuntimeException e) {
            return;
        }

        while (response.hasNext()){
            med.add(response.next());
            }

    }


    public List<MedicationPlanDTO> getGoodIntakes(){

        List<MedicationPlanDTO> listToReturn=new ArrayList<>();
        LocalTime now = LocalTime.now();
        for(MedicationPlanResponse medication : med){
            LocalTime startInterval = LocalTime.parse(medication.getIntakeStart()
                    , DateTimeFormatter.ofPattern("H:mm:ss"));
            LocalTime endInterval = LocalTime.parse(medication.getIntakeEnd()
                    , DateTimeFormatter.ofPattern("H:mm:ss"));
            if(startInterval.compareTo(now) < 0 && endInterval.compareTo(now) > 0){
                listToReturn.add(new MedicationPlanDTO(medication.getStartEndTime(),medication.getMedicationName(),medication.getPatientName(),medication.getIntakeStart(),medication.getIntakeEnd()));
            }
        }
        return listToReturn;
    }

}
