import {Component, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {MedPlans} from '../models/medPlans';
import {MedPlanServiceService} from '../service/med-plan-service.service';

@Component({
  selector: 'app-medication-plan-list',
  templateUrl: './medication-plan-list.component.html',
  styleUrls: ['./medication-plan-list.component.css']
})
export class MedicationPlanListComponent implements OnInit {

  medPlans: MedPlans[];
  today: number;

  constructor(private medPlanService: MedPlanServiceService) {
  }

  ngOnInit() {
    setInterval( () => {
      this.reloadData();
      this.today = Date.now();
  }, 1000);
  }

  reloadData() {
     this.medPlanService.getMedicationPlans().subscribe(data => this.medPlans = data);
  }

}
