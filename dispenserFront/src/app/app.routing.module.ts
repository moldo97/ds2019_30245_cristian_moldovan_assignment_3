import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';
import {MedicationPlanListComponent} from './medication-plan-list/medication-plan-list.component';

const routes: Routes = [
  {path: '', redirectTo: 'dispenser', pathMatch: 'full'},
  {path: 'dispenser', component: MedicationPlanListComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
