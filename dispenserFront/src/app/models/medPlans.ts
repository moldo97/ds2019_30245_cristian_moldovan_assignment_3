export class MedPlans {
  startEndTime: string;
  medicationName: string;
  patientName: string;
  // tslint:disable-next-line:variable-name
  intake_start: string;
  // tslint:disable-next-line:variable-name
  intake_end: string;
}
